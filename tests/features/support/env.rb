require 'selenium-webdriver'
require 'capybara/cucumber'
require 'site_prism'
require_relative 'page_helper.rb'

World(PageObjects)

Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.app_host = "https://neon.com.br"
    config.default_max_wait_time = 5
end