Dir[File.join(File.dirname(__FILE__), '../pages/*_page.rb')].each { |file| require file}

module PageObjects 
    def home 

        @home ||= Home.new

    end

    def perguntas 

        @perguntas ||= Perguntas.new

    end

    def statusServico

        @statusServico ||= Status.new

    end
end