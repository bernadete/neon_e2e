Dado("que estou no site neon") do
    home.load
  end

  Dado("seleciono a opção de Status") do
    home.linkStatus.click
  end
  
  Quando("verifico a serviço {string}") do |servico|
    page.has_text?(servico)
    statusServico.serAtivacaoCartao.hover
    
  end
  
  Então("o sistema exibe o status Ok") do
    tooltip = statusServico.mensagemOk
    expect(tooltip['data-tooltip-text']).to eql ('Funcionando normalmente')

  end
  