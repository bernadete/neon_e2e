Dado("que estou no site da neon") do
    home.load
  end
  
  Quando("acesso o menu de perguntas") do
   home.menuPerguntas.click
  end

  Quando("localizo a minha dúvida {string}") do |duvida|
   page.has_text?(duvida)
   perguntas.duvida.click
  end
  
  Então("verifico a resposta já respondida") do
    page.assert_text(text, perguntas.resposta1.text)
    page.assert_text(text, perguntas.resposta2.text)
  end

  