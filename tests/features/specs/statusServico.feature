#language: pt

@status
Funcionalidade: Exibir status do serviços neon

Cenario: Verificar que serviço de ativação de conta está funcionando

Dado que estou no site neon
E seleciono a opção de Status
Quando verifico a serviço "Ativação de conta"
Então o sistema exibe o status Ok



