class Perguntas < SitePrism::Page
   set_url '/perguntas'

   element :duvida, 'ul:nth-child(10) li:nth-child(3) label h4'
   element :resposta1, 'ul:nth-child(10) li:nth-child(3) div div p:nth-child(1)'
   element :resposta2, 'ul:nth-child(10) li:nth-child(3) div div p:nth-child(3)'

end