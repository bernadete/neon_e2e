class Status < SitePrism::Page
    set_url '/atualizacoes/status/'

   element :serAtivacaoCartao, 'div:nth-child(13) div p'
   element :mensagemOk, 'div:nth-child(13) div'

end